import numpy as np
import gym

def descretizedStateSpace(hight, low, stateCountPos, stateCountSpeed):
    descrState = (hight - low) * np.array([stateCountPos, stateCountSpeed])
    return np.round(descrState, 0).astype(int)

# Define Q-learning function
def startAgent(pathToAgent, episodes, stateCountPos, stateCountSpeed): 
   
    env = gym.make('MountainCar-v0')
    env.reset()

    # Initialize Q table
    Q = np.load(pathToAgent)
    
    # Initialize variables to track rewards
    rewardList = []
    aveRewardList = []
    
    # Run Q learning algorithm
    for i in range(episodes):
        # Initialize parameters
        done = False
        totReward, reward = 0,0
        state = env.reset()
        
        # Discretize state
        stateAdj = descretizedStateSpace(state, env.observation_space.low, stateCountPos, stateCountSpeed)
    
        while done != True:   
            # Render environment for last five episodes
            if i >= (episodes - 10):
                env.render()
                
            # Determine next action - epsilon greedy strategy
            action = np.argmax(Q[stateAdj[0], stateAdj[1]]) 

            # Get next state and reward
            state2, reward, done, info = env.step(action) 
            
            # Discretize state2
            state2Adj = descretizedStateSpace(state2, env.observation_space.low, stateCountPos, stateCountSpeed)
              
            # Update variables
            totReward += reward
            stateAdj = state2Adj
          
        # Track rewards
        rewardList.append(totReward)
        
        if (i+1) % 100 == 0:
            aveReward = np.mean(rewardList)
            aveRewardList.append(aveReward)
            rewardList = []
            
        if (i+1) % 100 == 0:    
            print('Episode {} Average Reward: {}'.format(i+1, aveReward))
            
    env.close()


startAgent('qLearnResult.npy', 200, 10, 100)

