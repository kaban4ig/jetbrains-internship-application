Логика агента реализована с помощью Q learning алгоритма
Файлы qLearnResult.npy и qLearnResultContinuous.npy содержат Q матрицу для агента.
При необходимости можно провести повторное обучение с помощью скриптов MountainCar-v0-train.py и MountainCarContinuous-v0-train.py
Увидеть работу агента можно с помощью MountainCar-v0-result.py или MountainCarContinuous-v0-result.py,
которые используют qLearnResult.npy и qLearnResultContinuous.npy соответственно(играется 200 эпиходов, показываются последнии 10).