import numpy as np
import gym
from pandas import DataFrame

def getDefaultReportParams(learning, discount, epsilon, minEps, episodes, reductionFactor, stateCountPos, stateCountSpeed):
    return {
        'learning': learning, 
        'discount': discount, 
        'epsilon': epsilon, 
        'minEps': minEps, 
        'episodes': episodes, 
        'reductionFactor': reductionFactor, 
        'stateCountPos': stateCountPos, 
        'stateCountSpeed': stateCountSpeed,
        'mean': 0.0
    }

def descretizedStateSpace(hight, low, stateCountPos, stateCountSpeed):
    descrState = (hight - low) * np.array([stateCountPos, stateCountSpeed])
    return np.round(descrState, 0).astype(int)

# Define Q-learning function
def QLearning(learning, discount, epsilon, minEps, episodes, reductionFactor, stateCountPos, stateCountSpeed): 

    resultInfo = getDefaultReportParams(learning, discount, epsilon, minEps, episodes, reductionFactor, stateCountPos, stateCountSpeed)

    env = gym.make('MountainCar-v0')
    env.reset()
    # Determine size of discretized state space
    numStates = descretizedStateSpace(env.observation_space.high, env.observation_space.low, stateCountPos, stateCountSpeed) + 1 #round can add 1 in the next descritizations
    
    # Initialize Q table
    Q = np.random.uniform(low = -1, high = 1, 
                          size = (numStates[0], numStates[1], 
                                  env.action_space.n))
    
    # Initialize variables to track rewards
    rewardList = []
    
    # Calculate episodic reduction in epsilon
    reduction = (epsilon - minEps)/episodes*reductionFactor
    
    # Run Q learning algorithm
    for i in range(episodes):
        # Initialize parameters
        done = False
        totReward, reward = 0,0
        state = env.reset()
        
        # Discretize state
        stateAdj = descretizedStateSpace(state, env.observation_space.low, stateCountPos, stateCountSpeed)
    
        while done != True:
            # Determine next action - epsilon greedy strategy
            if np.random.random() < 1 - epsilon:
                action = np.argmax(Q[stateAdj[0], stateAdj[1]]) 
            else:
                action = np.random.randint(0, env.action_space.n)
                
            # Get next state and reward
            state2, reward, done, info = env.step(action) 
            
            # Discretize state2
            state2Adj = descretizedStateSpace(state2, env.observation_space.low, stateCountPos, stateCountSpeed)
            
            #Allow for terminal states
            if done and state2[0] >= 0.5:
                Q[stateAdj[0], stateAdj[1], action] = reward
            # Adjust Q value for current state
            else:
                delta = learning*(reward + discount*np.max(Q[state2Adj[0], state2Adj[1]]) - Q[stateAdj[0], stateAdj[1],action])
                Q[stateAdj[0], stateAdj[1],action] += delta
                                     
            # Update variables
            totReward += reward
            stateAdj = state2Adj
        
        # Decay epsilon
        if epsilon > minEps:
            epsilon -= reduction
        
        # Track rewards
        rewardList.append(totReward)
            
    env.close()
    resultInfo['mean'] = np.mean(rewardList[-100:])
    return {
        'resultInfo': resultInfo,
        'qLearnResult': Q
    }


learning = 0.2
discount = 0.9
epsilon = 0.8
episode = 15000
reductionFactor = 8
stateCountPos = 10
stateCountSpeed = 100
result = QLearning(learning, discount, epsilon, 0, episode, reductionFactor, stateCountPos, stateCountSpeed)

df = DataFrame(columns= ['learning', 'discount', 'epsilon', 'minEps', 'episodes', 'reductionFactor', 'stateCountPos', 'stateCountSpeed', 'mean'])
df = df.append(result.get('resultInfo'), ignore_index=True)
df.to_csv("paramsInfo.csv")
np.save("qLearnResult", result.get('qLearnResult'))

"""
#So long ;(
# Run Q-learning algorithm
for learningInd in np.arange(0.1, 1, 0.1):
    for discountInd in np.arange(0.1, 1, 0.1):
        for epsilonInd in np.arange(0.1, 1, 0.1):
            for episodeInd in range(5000,20001,5000):
                df.to_csv("D:/paramsInfo.csv")
                print('{0} - {1} - {2} - {3}'.format(learningInd, discountInd, epsilonInd, episodeInd))
                for reductionFactorInd in range(1,15,4):
                    for stateCountPosInd in range(10,51,10):
                        for stateCountSpeedInd in range(100,1001,100):
                            paramsInfo = QLearning(learningInd, discountInd, epsilonInd, 0, episodeInd, reductionFactorInd, stateCountPosInd, stateCountSpeedInd)
                            df = df.append(paramsInfo, ignore_index=True)
df.to_csv("D:/paramsInfo.csv")
"""